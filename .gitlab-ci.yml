include: .gitlab-ci-variables.yml

stages:
  - build
  - upload

workflow: # don't run branch pipelines except for main branch
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" # merge request pipeline
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH # branch pipeline only for main

build:
  stage: build
  image: node:lts-alpine
  variables:
    GIT_BRANCH: $CI_COMMIT_REF_NAME
  before_script:
    - yarn
  script:
    - export DOCUSAURUS_BASE_URL="/$PROJECT_SLUG/" && [[ "$CI_COMMIT_REF_NAME" != "main" ]] && export DOCUSAURUS_BASE_URL="/$PROJECT_SLUG/!$CI_MERGE_REQUEST_IID/"
    - export GITLAB_SERVER_URL=$CI_SERVER_URL # used by docusaurus (see docusaurus.config.js)
    - export GITLAB_PROJECT_PATH=$CI_PROJECT_PATH # used by docusaurus (see docusaurus.config.js)
    - yarn docusaurus parse
    - yarn docusaurus glossary
    - yarn build
  artifacts:
    expose_as: 'Built Documentation'
    paths:
      - build/
    expire_in: 30 days
  rules:
    - when: always

upload:preview:
  stage: upload
  variables:
    GIT_STRATEGY: none # don't clone git repo
  environment:
    name: preview/$CI_MERGE_REQUEST_IID
    on_stop: stop:preview
    auto_stop_in: 2 week
    url: https://preview.docs.fitko.dev/$PROJECT_SLUG/!$CI_MERGE_REQUEST_IID
  script:
    - echo "Hey, my name is $CI_JOB_ID! I'm just here so that the upload to the preview environment is triggered."
    - echo "That's a very important task and I'm very proud to help you finish that great feature you're building! :D"
    - echo "There is nothing left to do here for me. Deployment will be triggered via a GitLab webhook."
    - echo "Bye, bye! 👋"
  needs:
    - job: build
      artifacts: false # don't fetch artifacts from build job
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH

upload:production:
  stage: upload
  variables:
    GIT_STRATEGY: none # don't clone git repo
  environment:
    name: production
    url: https://docs.fitko.de/$PROJECT_SLUG/
  script:
    - echo "Hey, my name is $CI_JOB_ID! I'm just here so that the upload to the preview environment is triggered."
    - echo "That's a very important task and I'm very proud to help you finish that great feature you're building! :D"
    - echo "There is nothing left to do here for me. Deployment will be triggered via a GitLab webhook."
    - echo "Bye, bye! 👋"
  needs:
    - job: build
      artifacts: false # don't fetch artifacts from build job
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

stop:preview:
  stage: .post
  variables:
    GIT_STRATEGY: none # don't clone git repo
  environment:
    name: preview/$CI_MERGE_REQUEST_IID
    action: stop
  script:
    - echo "Hey, my name is $CI_JOB_ID! I'm just here so that the preview environment gets stopped."
    - echo "That's a very important task and I'm very proud to help you finish that great feature you're building! :D"
    - echo "There is nothing left to do here for me. Deployment will be triggered via a GitLab webhook."
    - echo "Bye, bye! 👋"
  needs:
    - job: upload:preview
      artifacts: false # don't fetch artifacts from upload:preview job
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: manual
