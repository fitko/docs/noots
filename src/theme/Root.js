import React from 'react'
import NavigationHeader from '../components/NavigationHeader'

export default function Root({children}) {
  return <>
    <NavigationHeader />
    <main className='sm:py-1'>
      {children}
    </main>
  </>;
}