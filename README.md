# Dokumentation des Nationalen Once-Only-Technical-System

## Lokale Entwicklung
To preview your changes as you edit the files, you can run a local development server that will serve your website and reflect the latest changes.

```shell
$ yarn
$ yarn start
```

See https://docusaurus.io/docs/installation#running-the-development-server for details.

## Dokumentation des Inhalts

Für Inhalte können generell Markdown-Dateien verwendet werden. Markdown ist eine einfache Art und Weise Texte einfach und schnell zu formatieren [(mehr)](https://www.markdownguide.org/getting-started/). Diese Dateien Enden auf `.md`. Daneben gibt es noch Dateien mit der Endung `.mdx`. Diese nutzen MDX, eine Erweiterung von Markdown, um JSX-Komponenten (in diesem Fall React Komponenten) in das Markdown einzubetten [(mehr)](https://mdxjs.com).

Ein Beispiel hierfür ist die Nutzung der ApiSpec-Komponente, die erlaubt eine OpenAPI-Spezifikation via RapiDoc einzubetten.


```jsx
import ApiSpec from '@site/src/components/ApiSpec'

<ApiSpec />
```

### Versionierte Dokumentation

Versionierung der Dokumentation ist [hier](https://docusaurus.io/docs/versioning) beschrieben.

### Die Navigationselemente

Navigationselemente werden in der Datei `docusaurus.config.js` unter `themeConfig > navbar` konfiguriert. Die weitere bzw. genauere Nutzung der Navigationsleiste ist [hier](https://docusaurus.io/docs/versioning) beschrieben.

### Die Seitennavigation

Die Seitennavigation ist in `sidebar.js` definiert. Hierin können Einträge/slugs/DocumentIds gruppiert und aufgelistet werden. Die Seitenleiste wird nur auf Seiten angezeigt, die auch in der Seitennavigationsdefinition aufgeführt sind.

Weitere Informationen sind in der [Dokumentation](https://docusaurus.io/docs/sidebar) zu finden.


## Das Glossar

Die Dokumentation kann durch ein automatisch erstelltes Glossar ergänzt werden. Hierzu wurde das `docusaurus/terminology` Plugin eingebunden. Das Plugin erstellt eine verlinkte Liste aus einzelnen Begriffen und ergänzt automatisch Links und Schnellinformationen (per Tooltipp) in den jeweiligen Dokumenten. Dazu muss eine bestimmte Struktur eingehalten werden.

### Begriff erstellen

Damit die automatische Erstellung funktioniert, sollte jeder Glossar-Begriff sein eigenes `.md(x)` Dokument
im `./docs/terms` Verzeichnis haben. Die Struktur ist wie folgt:

```markdown
---
id: term_name
title: Titel der Seite
hoverText: Eine Kurzbeschreibung des Begriffs, wird bei Mouse-Over angezeigt.
---

### Begriff

Ausführliche inhaltliche Beschreibung folgt hier...
```

> Achtung: Die `hoverText` und `id` Attribute werden zwingend benötigt. Es bietet sich an das Dokument nach `term_name` zu benennen.

### Verwendung von Mustern zur Referenzierung

Um einen Glossar-Begriff in einem anderen `docs/*.md(x)` Dokument zu referenzieren, wird folgendes Muster verwendet:

```
%%term_text|term_name%%
```

wobei:
- `term_text`: Der Begriffs-Text der in der Dokumentationsseite sichtbar sein soll.
- `term_name`: Der Wert des eindeutigen `id` Attribut, das im `.md(x)` des Begriffs verwendet wird.

### Beispiel

Nehmen wir an in `./docs/terms/chocolate.md` ist ein Begriff mit `id: chocolate` definiert. Der Begriff kann von jeder Seite der Dokumentation aus wie folgt referenziert werden:

```markdown
Viele Leute fragen sich, wie %%Schokolade|chocolate%% eigentlich hergestellt wird...
```

Das Plugin wird das Muster mit diesem Inhalt ersetzen:

```html
Viele Leute fragen sich, wie <Term reference="chocolate" popup="Kurzbeschreibung von Schokolade aus der Datei chocolate.md">Schokolade</Term> eigentlich hergestellt wird...
```

Die &lt;Term&gt; React-Komponente enthält dann den Link zum Glossar und eine Tooltipp-Funktionalität.

> Achtung: Das Script sollte nicht lokal ausgeführt werden, da es die Markdown-Dokumente verändert. Diese Änderungen sollten nie ins Git gelangen, sonst funktioniert die Mustererkennung bei zukünftigen Veränderungen nicht mehr.

### Lokales testen der Änderungen

Wenn die Begriffe und Muster in `.md` Dateien angelegt sind, kann lokal das Plugin im `dry-run` Modus ausgeführt werden. So werden mögliche Kompilierungsfehler angezeigt und eine Beispiel-Ausgabe aller Änderungen, die das Plugin vornehmen würde:

```commandline
yarn docusaurus parse --dry-run
```

> Hinweis: Aufgrund eines Bugs im Plugin kann es auf Windows-Systemen vorkommen, dass das lokale ausführen einen Fehler `⚠ No term files found` wirft. Dies kann umgangen werden indem man das Linux Subsystem nutzt und dort entwickelt.

### Mögliche Probleme bei der Einbindung

* Das `docusaurus/terminology` Plugin funktioniert momentan nur einwandfrei in Dokumenten, die über einen Header-Bereich verfügen!

  *Lösung*: in betroffenen Dateien einen Header ergänzen.

  ```markdown
  ---
  id: Meine Seite
  ---
  ```

* Je nach Konfiguration der Routing können evtl. die Links auf die Begriffe nicht funktionieren, Grund ist ein Bug im Plugin.

  *Lösung*: den Docusaurus Config-Parameter `routeBasePath` auf `'docs'` setzen.

* Nach obiger Änderung kann es zu einem Fehler beim Build-Vorgang, besser gesagt beim prüfen der Links danach kommen.

  *Lösung*: Prüfen ob eine Startseite für die `/` Route vorhanden ist (z.B. `src/pages/index.mdx`). Diese kann auch einfach nur einen Redirect auf die Startseite der Dokumentation unter `docs/` enthalten.

* Die automatisch erstellte Glossar-Seite erscheint nicht in der Sidebar.

  *Lösung*: Die Vorlage des `glossary.md` muss einen Title enthalten. Dieses Projekt enthält eine Referenz-Implementierung.

### Erzeugung der Glossar-Docs in der CI

Da das Glossar-Plugin Markdown-Dateien verändert, sollen diese veränderten Dateien nie ins Git gelangen. Stattdessen sollte lokal nur mit dem `dry-run` Modus getestet werden und die eigentliche Erzeugung des Glassars in der Gitlab-Ci stattfinden.

Dazu wurden folgende Schritte in der Gitlab-CI hinzugefügt:

```yaml
...
build:
  ...
  script:
    - ...
    - yarn docusaurus parse
    - yarn docusaurus glossary
    - yarn build
  ...
```


## Deployment via CI/CD

Das Deployment ist aktuell in `.gitlab-ci.yml` vordefiniert. Sie funktioniert, wie folgt:

![CI/CD](cicd.png)

In einem ersten Schritt wird die Dokumentation immer gebaut und überprüft. Zur Überprüfung gehört hier u. a. die Validierung interner Links bzw. korrekte Bild-URLs.

Danach wird im Falles des Standard-Branches (`main`) diese gebaute Version live veröffentlicht, aktuell auf Uberspace. Ist der betroffene Branch nicht der `main`-Branch, dann wird die gebaute Version in einer Review-Umgebung veröffentlicht, um sie z. B. vor einem Merge zentral zu überprüfen.

Der hier als letztes dargestellte Job `stop:preview` wird ausgeführt, wenn der Merge-Request geschlossen wurde, der zugehörige Branch gelöscht wurde oder die Lebenszeit der Umgebung abgelaufen ist (diese ist in `.gitlab-ci.yml` auf 14 Tage festgelegt). Sollte die Review-Umgebung dennoch noch einmal notwendig sein, so kann sie manuell über die Weboberfläche von Gitlab deployed werden.

Wenn die CI/CD Konfiguration angepasst werden soll, dann kann entsprechend der [Gitalb CI Dokumentation](https://docs.gitlab.com/ee/ci/) eine eigene Konfiguration in der `.gitlab-ci.yml` definiert werden.


## Erstellen von Diagrammen mit Mermaid

Diagramme können mit [Mermaid.js](https://mermaid.js.org/intro/) direkt in der Dokumentation erstellt werden. Dazu legt man einfach einen Code Block mit `mermaid` als Sprache an:
````
```mermaid
  sequenceDiagram

  # Participants
  participant A as Alice
  participant B as Bob

  # Relations
  A ->> B: Hi Bob!
  B ->> A: Nice to meet you, Alice
```
````

# Lokale Apache-Testumgebung für Docusaurus

Diese Docker-Konfiguration ermöglicht das lokale Testen von Docusaurus-Builds inklusive .htaccess-Regeln.

## Voraussetzungen

- Node.js
- Optional: Yarn Package manager (npm kann auch verwendet werden)
- Docker & Docker Compose (beides in Docker Desktop enthalten)
- Docusaurus Build im `build/` Verzeichnis
- .htaccess-Datei im `static/` Verzeichnis

## Schnellstart

1. Build der Docusaurus-Seite erstellen:
```bash
$ yarn install
$ yarn build
```

2. Docker-Container starten:
```bash
$ docker-compose up --build
```

Die Seite ist dann unter `http://localhost:8090` erreichbar.

## Projektstruktur

```
.
├── build/             # Docusaurus Build-Output
├── static/
│   └── .htaccess      # Apache-Konfiguration
├── docker-compose.yml
├── Dockerfile
└── my-httpd.conf      # Apache-Hauptkonfiguration
```

## Entwicklung

- Änderungen an der `.htaccess` und am am Build-Output werden durch das Volume-Mounting sofort wirksam

## Steuerung

```bash
# Container im Vordergrund starten
docker-compose up --build

# Container im Hintergrund starten
docker-compose up -d --build

# Container stoppen
docker-compose down
```

# License
Source code is licensed under the [EUPL](./LICENSE).

Unless stated otherwise, the content of this website is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).
