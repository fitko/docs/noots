module.exports = {
  defaultSidebar: [
    {
      type: 'category',
      label: 'Überblick',
      collapsible: false,
      items: [
        'Einleitung',
      ]
    },
    'SDK',
    'spec',
    'glossary'
  ]
}
