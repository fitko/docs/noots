FROM httpd:2.4-alpine

COPY my-httpd.conf /usr/local/apache2/conf/httpd.conf
COPY build/ /usr/local/apache2/htdocs/
COPY static/.htaccess /usr/local/apache2/htdocs/.htaccess